unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent;

type
  TForm1 = class(TForm)

    ListBox1: TListBox;
    Button1: TButton;
    NetHTTPClient: TNetHTTPClient;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
    TPersonagem = class(TObject)
    nome, profissao  : string;
    vida ,nivel, defesa, ataque:integer;
    end;

var
  Form1: TForm1;
  personagem : TPersonagem;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var conteudo :string;
    listapersonagens : TArray<String>;
    listapessoas :  TArray<String>;
    stringPersonagem :string;
begin

       conteudo :=  NetHTTPClient.Get('https://venson.net.br/ws/personagens').ContentAsString();
     listaPersonagens := conteudo.Split(['&']);

        for stringPersonagem in listaPersonagens do
            begin

            personagem := TPersonagem.Create;

               listaPessoas := stringPersonagem.Split([';']);
               Personagem.nome := listapessoas[0];
               Personagem.profissao:= listapessoas[1];
               Personagem.nivel:= listapessoas[2].ToInteger;
               Personagem.ataque:= listapessoas[3].ToInteger;
               Personagem.defesa:= listapessoas[4].ToInteger;
               Personagem.vida:= listapessoas[5].ToInteger;


               ListBox1.Items.AddObject(personagem.nome, personagem);
            end;
end;



procedure TForm1.ListBox1DblClick(Sender: TObject);
 var Personagem : TPersonagem ;
begin
     Personagem := TPersonagem(Listbox1.Items.Objects[Listbox1.ItemIndex]);
     showmessage(Personagem.nome + ' ' + Personagem.profissao + '  ' +
         inttostr(Personagem.nivel));
end;

end.
